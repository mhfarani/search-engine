import json
from io import BytesIO

import requests
import torch
from PIL import Image
from clip import clip
from qdrant_client import QdrantClient
from qdrant_client.http.models import PointStruct, VectorParams, Distance
from tqdm import tqdm

# import requests
# from PIL import Image
# from io import BytesIO
# import torch

# from tqdm import tqdm
# import pinecone

# Initialize CLIP model
device = "cpu"
model, preprocess = clip.load("ViT-B/32", device=device)


client = QdrantClient(host="localhost", port=6333)
client.recreate_collection(
    collection_name="products",
    vectors_config=VectorParams(size=512, distance=Distance.COSINE),
)

with open('data.json') as f:
    products = json.load(f)


def process_and_index_products(products):
    point_id = 1  # Initialize point ID counter
    for product in tqdm(products):
        images = product['images']

        # Encode images
        for img_url in images:
            response = requests.get(img_url, timeout=20)
            image = Image.open(BytesIO(response.content))
            image_input = preprocess(image).unsqueeze(0).to(device)
            with torch.no_grad():
                image_features = model.encode_image(image_input).cpu().numpy().flatten().tolist()

            # Store in Qdrant
            client.upsert(
                collection_name="products",
                points=[
                    PointStruct(
                        id=point_id,  # Use integer ID
                        vector=image_features,
                        payload={"name": product['name'], "image_url": img_url}
                    )
                ]
            )
            point_id += 1  # Increment point ID


# Process and index all products
process_and_index_products(products)