from backend.settings import QDRANT_PORT, QDRANT_HOST
import clip
from qdrant_client import QdrantClient
from qdrant_client.http.models import PointStruct, VectorParams, Distance

class Clip:

    device = 'cpu'
    model, preprocess = clip.load("ViT-B/32", device=device)


class Qdrant:
    def __init__(self):
        self.client.recreate_collection(
            collection_name="products",
            vectors_config=VectorParams(size=512, distance=Distance.COSINE),
        )

    client = QdrantClient(host=QDRANT_HOST, port=QDRANT_PORT)