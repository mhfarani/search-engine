// frontend/src/App.js

import React, { useState } from 'react';
import axios from 'axios';

const App = () => {
  const [query, setQuery] = useState('');
  const [results, setResults] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  const handleSearch = async () => {
    if (!query) {
      setError('Please enter a search query');
      return;
    }

    setLoading(true);
    try {
      const response = await axios.post('http://127.0.0.1:8000/search/', { input: query });
      setResults(response.data.response);
      setError('');
    } catch (error) {
      setError('Error fetching search results. Please try again.');
      console.error('Error fetching search results', error);
    } finally {
      setLoading(false);
    }
  };

  // Function to chunk array into groups of size n
  const chunkArray = (array, size) => {
    return array.reduce((acc, _, i) => (i % size ? acc : [...acc, array.slice(i, i + size)]), []);
  };

  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', minHeight: '100vh', padding: '20px' }}>
      <h1 style={{ marginBottom: '20px' }}>Product Search</h1>

      <div style={{ width: '50%', marginBottom: '20px' }}>
        <input
          type="text"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
          placeholder="Enter search query"
          style={{ width: '100%', padding: '10px', fontSize: '16px' }}
        />
        <button onClick={handleSearch} disabled={loading} style={{ marginLeft: '10px', padding: '10px 20px', fontSize: '16px' }}>
          {loading ? 'Searching...' : 'Search'}
        </button>
      </div>

      {error && <p style={{ color: 'red', marginBottom: '20px' }}>{error}</p>}

      <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center', width: '100%' }}>
        {chunkArray(results, 3).map((chunk, index) => (
          <div key={index} style={{ display: 'flex', justifyContent: 'space-around', marginBottom: '20px', width: '60%' }}>
            {chunk.map((result) => (
              <div key={result.id} style={{ padding: '10px', border: '1px solid #ccc', borderRadius: '5px', width: '30%', textAlign: 'center' }}>
                <h2 style={{ marginBottom: '10px' }}>{result.payload.name}</h2>
                <img src={result.payload.image_url} alt={result.payload.name} style={{ maxWidth: '100%', height: 'auto', borderRadius: '5px' }} />
              </div>
            ))}
          </div>
        ))}
      </div>
    </div>
  );
};

export default App;
  