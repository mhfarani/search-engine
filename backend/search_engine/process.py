import json
from io import BytesIO
from typing import List

import requests
import torch
from PIL import Image
from qdrant_client.http.models import PointStruct

from search_engine.utils import Clip, Qdrant


class Process:
    point_id = 1
    qdrant = Qdrant()

    @classmethod
    def read_data_file(cls) -> List:
        with open('data.json') as f:
            products = json.load(f)
        return products

    @classmethod
    def process_and_index_image(cls, image_url: str):
        model = Clip.model
        preprocess = Clip.preprocess
        device = Clip.device

        response = requests.get(image_url, timeout=20)
        image = Image.open(BytesIO(response.content))
        image_input = preprocess(image).unsqueeze(0).to(device)
        with torch.no_grad():
            image_features = model.encode_image(image_input).cpu().numpy().flatten().tolist()
        return image_features

    @classmethod
    def store_image_in_qdrant(cls, image_features, image_url, name):

        cls.qdrant.client.upsert(
            collection_name="products",
            points=[
                PointStruct(
                    id=cls.point_id,
                    vector=image_features,
                    payload={"name": name, "image_url": image_url}
                )
            ]
        )
        cls.point_id += 1

    @classmethod
    def handle_encoding_and_save_image_in_qdrant(cls):
        products = cls.read_data_file()

        for product in products:
            product_name = product['name']
            product_images = product['images']
            for image_rl in product_images:
                image_feature = cls.process_and_index_image(image_rl)
                cls.store_image_in_qdrant(image_feature, image_rl, product_name)
