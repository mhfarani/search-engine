import torch

import clip

from search_engine.utils import Clip, Qdrant


class Search:
    @classmethod
    def search_products(cls, search_input):
        text_inputs = clip.tokenize([search_input]).to(Clip.device)
        with torch.no_grad():
            query_features = Clip.model.encode_text(text_inputs).cpu().numpy().flatten().tolist()

        search_result = Qdrant.client.search(
            collection_name="products",
            query_vector=query_features,
            limit=10
        )

        results = [{"id": res.id, "score": res.score, "payload": res.payload} for res in search_result]
        return results
