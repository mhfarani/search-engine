from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import serializers

from search_engine.search import Search


class SearchSerializer(serializers.Serializer):
    input = serializers.CharField(required=True)
# Create your views here.

class SearchApiView(APIView):
    def post(self,request):
        serializer = SearchSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = Search.search_products(serializer.validated_data['input'])
        return Response({"response" :response})
            # Search.search_products()



